import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RespuestaPokemons, RespuestaDetallePokemons, RespuestaTipos, RespuestaDetalleTipos, RespuestaObjetos, RespuestaDetalleObjetos } from '../interfaces/interfaces';
@Injectable({
  providedIn: 'root'
})
export class DataService {
  page = -25
  tipos = -1
  constructor(private http: HttpClient) { }

  getPokemons() {
    this.page += 25
    return this.http.get<RespuestaPokemons>(`https://pokeapi.co/api/v2/pokemon?limit=25&offset=${this.page}`)

  }

  getDetallePokemons(url) {
    return this.http.get<RespuestaDetallePokemons>(url)
  }

  getTipos() {
    this.tipos += 1
    return this.http.get<RespuestaTipos>(`https://pokeapi.co/api/v2/type?limit=20&offset=${this.tipos}`)
  }

  getDetalleTipos(url) {
    return this.http.get<RespuestaDetalleTipos>(url)
  }

  getObjetos() {
    this.page += 25
    return this.http.get<RespuestaObjetos>(`https://pokeapi.co/api/v2/item?limit=25&offset=${this.page}`)

  }

  getDetalleObjetos(url) {
    return this.http.get<RespuestaDetalleObjetos>(url)
  }

}
