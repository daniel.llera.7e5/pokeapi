import { Injectable } from '@angular/core';
import { DetallePokemonComponent } from '../components/detalle-pokemon/detalle-pokemon.component';
import {Storage} from '@ionic/storage-angular';
import { Name, RespuestaDetallePokemons } from '../interfaces/interfaces';
@Injectable({
  providedIn: 'root'
})
export class DataLocalService {
  
  pokemons: RespuestaDetallePokemons[] = []

  private _storage: Storage | null = null;

  constructor(private storage: Storage) { 
    this.init();
  }

  async init() {
    const storage = await this.storage.create();
    this._storage = storage;
   }

   setData(pokemon){
    this.pokemons.push(pokemon)
    this.storage.set('pokemon', this.pokemons)
    console.log(pokemon)
 }

 async getData(){
  this.pokemons = await this.storage.get('pokemon')
  return this.pokemons;
}

delete(pokemon ){
  this.pokemons = this.pokemons.filter(poke=>poke.name!=pokemon.name)
  this.storage.set('pokemon', this.pokemons)
  console.log(pokemon.name)
  
  } 
}
