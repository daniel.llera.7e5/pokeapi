import { Component } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  tipos = [];

  constructor(private dataService: DataService) { }
  ngOnInit(): void {
    this.loadTipos();
  }
  loadTipos(event?) {
    this.dataService.getTipos().subscribe(
      resp => {
        console.log('Tipos', resp);
        //if (resp.next === null) {
        // event.target.disabled = true;
        // event.target.complete();
        //  return;
        //  }

        this.tipos.push(...resp.results);
        console.log(resp.next);
        if (event) {
          event.target.complete();
        }
      });
  }

  loaData(event) {
    this.loadTipos(event)
  }
}
