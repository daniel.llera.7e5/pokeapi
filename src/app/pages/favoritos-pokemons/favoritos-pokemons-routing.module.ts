import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FavoritosPokemonsPage } from './favoritos-pokemons.page';

const routes: Routes = [
  {
    path: '',
    component: FavoritosPokemonsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FavoritosPokemonsPageRoutingModule {}
