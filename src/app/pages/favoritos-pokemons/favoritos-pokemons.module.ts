import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FavoritosPokemonsPageRoutingModule } from './favoritos-pokemons-routing.module';

import { FavoritosPokemonsPage } from './favoritos-pokemons.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FavoritosPokemonsPageRoutingModule
  ],
  declarations: [FavoritosPokemonsPage]
})
export class FavoritosPokemonsPageModule {}
