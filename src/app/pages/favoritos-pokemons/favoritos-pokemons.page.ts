import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataLocalService } from '../../services/data-local.service';

@Component({
  selector: 'app-favoritos-pokemons',
  templateUrl: './favoritos-pokemons.page.html',
  styleUrls: ['./favoritos-pokemons.page.scss'],
})
export class FavoritosPokemonsPage implements OnInit {
  pokemon = [];
  constructor(private router: Router, private datalocal: DataLocalService) { }

  ngOnInit() {
    this.datalocal.getData().then(datalocal => {
      this.pokemon = datalocal;
      console.log(datalocal)
      console.log(this.pokemon)

    })
  }

  navigate() {
    this.router.navigateByUrl('/tabs');
  }

  delete(pokemon) {
    console.log(pokemon)
    this.datalocal.delete(pokemon);
  }

}
