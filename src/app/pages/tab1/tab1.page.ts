import { Component } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  pokemons = [];

  constructor(private dataService: DataService, private router: Router) { }
  ngOnInit(): void {
    this.loadPokemons();
  }
  loadPokemons(event?) {
    this.dataService.getPokemons().subscribe(
      resp => {
        console.log('Pokemons', resp);
        if (resp.next === null) {
          event.target.disabled = true;
          event.target.complete();
          return;
        }

        this.pokemons.push(...resp.results);
        console.log(resp.next);
        if (event) {
          event.target.complete();
        }
      });
  }

  loaData(event) {
    this.loadPokemons(event)
  }

  navigate() {
    this.router.navigateByUrl('/favoritos-pokemons');

  }
}

