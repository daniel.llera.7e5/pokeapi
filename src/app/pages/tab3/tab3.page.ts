import { Component } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  objetos = [];

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.loadObjetos();
  }

  loadObjetos(event?) {
    this.dataService.getObjetos().subscribe(
      resp => {
        console.log('Objetos', resp);
        if (resp.next === null) {
          event.target.disabled = true;
          event.target.complete();
          return;
        }

        this.objetos.push(...resp.results);
        console.log(resp.next);
        if (event) {
          event.target.complete();
        }
      });
  }

  loaData(event) {
    this.loadObjetos(event)
  }
}
