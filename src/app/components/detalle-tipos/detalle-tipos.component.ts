import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { RespuestaDetalleTipos } from 'src/app/interfaces/interfaces';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-detalle-tipos',
  templateUrl: './detalle-tipos.component.html',
  styleUrls: ['./detalle-tipos.component.scss'],
})
export class DetalleTiposComponent implements OnInit {
  @Input() url
  tipo: RespuestaDetalleTipos
  constructor(private modalCtrl: ModalController, private dataService: DataService) { }


  ngOnInit() {
    this.loadDetalleTipos();
  }

  regresar() {
    this.modalCtrl.dismiss()
  }

  loadDetalleTipos() {
    this.dataService.getDetalleTipos(this.url).subscribe(
      resp => {
        console.log('TiposDetalle', resp);
        this.tipo = resp
      }
    );
  }
}
