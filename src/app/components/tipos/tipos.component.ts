import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Tipos } from '../../interfaces/interfaces';
import { DetalleTiposComponent } from '../detalle-tipos/detalle-tipos.component';

@Component({
  selector: 'app-tipos',
  templateUrl: './tipos.component.html',
  styleUrls: ['./tipos.component.scss'],
})
export class TiposComponent implements OnInit {
  @Input() tipos: Tipos[] = [];

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() { }

  async verDetalle(url: string) {
    const modal = await this.modalCtrl.create({
      component: DetalleTiposComponent,
      componentProps: { url }
    })
    console.log("id" + url)
    modal.present()
  }

}
