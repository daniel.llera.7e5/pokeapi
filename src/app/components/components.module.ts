import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PokemonsComponent } from './pokemons/pokemons.component';
import { IonicModule } from '@ionic/angular';
import { DetallePokemonComponent } from './detalle-pokemon/detalle-pokemon.component';
import { TiposComponent } from './tipos/tipos.component';
import { DetalleTiposComponent } from './detalle-tipos/detalle-tipos.component';
import { ObjetosComponent } from './objetos/objetos.component';
import { DetalleObjetosComponent } from './detalle-objetos/detalle-objetos.component';



@NgModule({
  declarations: [
    PokemonsComponent,
    TiposComponent,
    DetallePokemonComponent,
    DetalleTiposComponent,
    ObjetosComponent,
    DetalleObjetosComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    PokemonsComponent,
    TiposComponent,
    DetallePokemonComponent,
    DetalleTiposComponent,
    ObjetosComponent,
    DetalleObjetosComponent
  ]
})
export class ComponentsModule { }
