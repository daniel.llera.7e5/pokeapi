import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Pokemons } from '../../interfaces/interfaces';
import { DetallePokemonComponent } from '../detalle-pokemon/detalle-pokemon.component';

@Component({
  selector: 'app-pokemons',
  templateUrl: './pokemons.component.html',
  styleUrls: ['./pokemons.component.scss'],
})
export class PokemonsComponent implements OnInit {

  @Input() pokemons: Pokemons[] = [];
  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {}

  async verDetalle(url: string){
    const modal = await this.modalCtrl.create({
      component: DetallePokemonComponent,
      componentProps: { url }
    })
    console.log("id" + url)
    modal.present()
  }
}
