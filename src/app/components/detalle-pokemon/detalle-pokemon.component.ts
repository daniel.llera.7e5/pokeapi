import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DataService } from '../../services/data.service';
import { Pokemons, RespuestaDetallePokemons } from '../../interfaces/interfaces';
import { DataLocalService } from '../../services/data-local.service';

@Component({
  selector: 'app-detalle-pokemon',
  templateUrl: './detalle-pokemon.component.html',
  styleUrls: ['./detalle-pokemon.component.scss'],
})
export class DetallePokemonComponent implements OnInit {
  @Input() url
  pokemon: RespuestaDetallePokemons

  constructor(private modalCtrl: ModalController, private dataService: DataService, private datalocal: DataLocalService) { }

  ngOnInit() {
    this.loadDetallePokemons();
  }

  regresar(){
    this.modalCtrl.dismiss()
  }

  loadDetallePokemons(){
    this.dataService.getDetallePokemons(this.url).subscribe(
      resp => {
        console.log('PokemonsDetalle', resp);
        this.pokemon = resp
        }
      );
  }

  llamaset(pokemon){
    this.datalocal.setData(pokemon);
  }
}
