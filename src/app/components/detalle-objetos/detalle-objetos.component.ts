import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { RespuestaDetalleObjetos } from 'src/app/interfaces/interfaces';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-detalle-objetos',
  templateUrl: './detalle-objetos.component.html',
  styleUrls: ['./detalle-objetos.component.scss'],
})
export class DetalleObjetosComponent implements OnInit {
  @Input() url
  objeto: RespuestaDetalleObjetos
  constructor(private modalCtrl: ModalController, private dataService: DataService) { }

  ngOnInit() {
    this.loadDetalleObjetos();
  }

  regresar() {
    this.modalCtrl.dismiss()
  }

  loadDetalleObjetos() {
    this.dataService.getDetalleObjetos(this.url).subscribe(
      resp => {
        console.log('PokemonsDetalle', resp);
        this.objeto = resp
      }
    );
  }

}
