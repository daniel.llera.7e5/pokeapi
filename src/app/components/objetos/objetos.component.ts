import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Objetos } from 'src/app/interfaces/interfaces';
import { DetalleObjetosComponent } from '../detalle-objetos/detalle-objetos.component';

@Component({
  selector: 'app-objetos',
  templateUrl: './objetos.component.html',
  styleUrls: ['./objetos.component.scss'],
})
export class ObjetosComponent implements OnInit {
  @Input() objetos: Objetos[] = [];
  constructor(private modalCtrl: ModalController) { }

  ngOnInit() { }

  async verDetalle(url: string) {
    const modal = await this.modalCtrl.create({
      component: DetalleObjetosComponent,
      componentProps: { url }
    })
    console.log("id" + url)
    modal.present()
  }
}
