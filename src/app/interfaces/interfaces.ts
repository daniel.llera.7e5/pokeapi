export interface RespuestaPokemons {
  count: number;
  next: string;
  previous?: any;
  results: Pokemons[];
}

export interface Pokemons {
  name: string;
  url: string;
}

//RespuestaTipos
export interface RespuestaTipos {
  count: number;
  next?: any;
  previous?: any;
  results: Tipos[];
}

export interface Tipos {
  name: string;
  url: string;
}

//RespuestaDetallePokemons
export interface RespuestaDetallePokemons {
  height: number;
  name: string;
  sprites: Sprites;
  stats: Stat[];
  weight: number;
}

export interface Type {
  slot: number;
}

export interface Stat {
  base_stat: number;
  effort: number;
}

export interface Sprites {
  front_default: string;
}

//RespuestaDetalleTipos
export interface RespuestaDetalleTipos {
  damage_relations: Damagerelations;
  game_indices: Gameindex[];
  generation: Doubledamagefrom;
  id: number;
  move_damage_class: Doubledamagefrom;
  moves: Doubledamagefrom[];
  name: string;
  names: Name[];
  past_damage_relations: any[];
  pokemon: Pokemon[];
}

export interface Pokemon {
  pokemon: Doubledamagefrom;
  slot: number;
}

export interface Name {
  language: Doubledamagefrom;
  name: string;
}

export interface Gameindex {
  game_index: number;
  generation: Doubledamagefrom;
}

export interface Damagerelations {
  double_damage_from: Doubledamagefrom[];
  double_damage_to: Doubledamagefrom[];
  half_damage_from: Doubledamagefrom[];
  half_damage_to: Doubledamagefrom[];
  no_damage_from: Doubledamagefrom[];
  no_damage_to: any[];
}

export interface Doubledamagefrom {
  name: string;
  url: string;
}

//RespuestaObjetos
export interface RespuestaObjetos {
  count: number;
  next: string;
  previous?: any;
  results: Objetos[];
}

export interface Objetos {
  name: string;
  url: string;
}

export interface RespuestaDetalleObjetos {
  attributes: Attribute[];
  baby_trigger_for?: any;
  category: Attribute;
  cost: number;
  effect_entries: Effectentry[];
  flavor_text_entries: Flavortextentry[];
  fling_effect?: any;
  fling_power?: any;
  game_indices: Gameindex[];
  held_by_pokemon: any[];
  id: number;
  machines: any[];
  name: string;
  names: Name[];
  sprites: Sprites;
}

export interface Sprites {
  default: string;
}

export interface Name {
  language: Attribute;
  name: string;
}

export interface Gameindex {
  game_index: number;
  generation: Attribute;
}

export interface Flavortextentry {
  language: Attribute;
  text: string;
  version_group: Attribute;
}

export interface Effectentry {
  effect: string;
  language: Attribute;
  short_effect: string;
}

export interface Attribute {
  name: string;
  url: string;
}